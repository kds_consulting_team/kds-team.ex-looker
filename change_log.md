**0.5.5**
Changed deployment script ot fetch configs.

**0.5.4**
Added proper primary key column sanitization.

**0.5.3**
Changed the destination bucket handling. Will be stripped of whitespaces by default.

**0.5.2**
Further changes to variable naming, added comments to functions.

**0.5.1**
Changed naming of the variables to avoid collisions with in-built functions

**0.5.0**
This is a migration of original private Looker extractor.
Added LICENSE.md
Added parameters check