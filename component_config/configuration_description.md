## Configuration description

Below is the description of input parameters and expected output.

### API Limitations
The extractor is limited by **limitations** of Looker API, which currently are:
* Pivoted look > 5000 rows - only unpivotted part will be extracted.
* Pivoted look <= 5000 rows - everything will be extracted.
* Non-pivoted look - everything will be extracted, regardless of # of rows.

To specify number of rows to be extracted, use `limit`. To download all rows, use `limit=-1`, even though data might be truncated due to above limitations.

### API secret and key
Obtaining the API secret and key is rather simple. In order to obtain the client secret and id, an admin access is required.
You can simply follow this [document](https://discourse.looker.com/t/using-the-looker-api-3/2988)
or the following steps:

1. Login to the relevant looker instance.

2. Navigate to **Admin** section (top right) and then **Users** in bar on the left.

3. Click on the edit button next to the relevant user.

4. Click on *Edit keys* in **API3 Keys** section.

5. Two scenarios can happen:
	* If API keys were generated previously, client id and secret will be displayed on screen.
	* If API keys were not generated previously, simply click on *New API3 Key* to generate the credentials.

6. Use obtained client id and secret to authorize the extractor.

### Input

Configuration schema accepts following parameters:
* **Client ID** - Client ID obtained in the API section of Looker dashboard.
* **Client Secret** - Client Secret obtained in the API section of Looker dashboard.
* **API Endpoint** - API Endpoint via which requests are sent.
* **Look ID** - ID of a look, from which the data should be downloaded. The look is ran automatically.
* **Destination Table** - A table in KBC Storage, where data will be loaded. If left blank, data will be downloaded to `in.c-looker.looker_data_id`, where `id` is equal to ID of a look.
* **Incremental Load** - Marks, whether load should be incremental.
* **Primary Key** - Comma-separated columns, which are to be used as PK.
* **Limit** - Row limit for look. See API Limitations above.

For more info about Looker API, see [Looker API Documentation](https://docs.looker.com/reference/api-and-integration/api-getting-started).

### Output

Table in specified location in KBC storage.