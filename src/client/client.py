import looker_sdk
from looker_sdk import api_settings


class LookerClientException(Exception):
    pass


class MyApiSettings(api_settings.ApiSettings):
    def __init__(self, base_url: str, client_id: str, client_secret: str, *args, **kwargs):
        self.base_url = base_url
        self.client_id = client_id
        self.client_secret = client_secret
        super().__init__(*args, **kwargs)

    def read_config(self):
        return {
            "base_url": self.base_url,
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "timeout": "1200"
        }


class LookerClient:
    def __init__(self, base_url, client_id, client_secret):
        self.client = looker_sdk.init40(config_settings=MyApiSettings(base_url, client_id, client_secret))

    def download_look(self, look_id, limit):
        return self.client.run_look(look_id, 'csv', limit=limit)
