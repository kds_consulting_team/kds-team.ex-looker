import csv
import re
import logging

import looker_sdk.error
from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException
from keboola.utils.helpers import comma_separated_values_to_list

from client import LookerClient, LookerClientException
from configuration import Configuration, LookerObject


class Component(ComponentBase):

    def __init__(self):
        super().__init__()
        self._configuration: Configuration
        self.client: LookerClient

    def run(self):
        self._init_configuration()
        self._init_client()

        for looker_object in self._configuration.looker_objects:
            try:
                self.download_look_object(looker_object)
            except looker_sdk.error.SDKError:
                raise UserException(f"Unable to download Looker object {looker_object}, please check credentials.")

    def _init_configuration(self) -> None:
        self.validate_configuration_parameters(Configuration.get_dataclass_required_parameters())
        self._configuration: Configuration = Configuration.load_from_dict(self.configuration.parameters)

    def _init_client(self) -> None:
        base_url = f"https://{self.extract_subdomain(self._configuration.api_endpoint)}.looker.com:19999"
        self.client = LookerClient(base_url=base_url,
                                   client_id=self._configuration.client_id,
                                   client_secret=self._configuration.pswd_client_secret)

    @staticmethod
    def full_match_re(pattern, string):
        """
        Function to return whether the match was full or not
        """
        return bool(re.fullmatch(pattern, string))

    def is_valid_keboola_destination(self, destination_string):
        return self.full_match_re(r'^(in|out)\.(c-)\w*\.[\w\-]*', destination_string)

    def get_destination(self, looker_object: LookerObject):
        if self.is_valid_keboola_destination(looker_object.output):
            destination = looker_object.output
            logging.debug(f"The table with id {looker_object.id} will be saved to {destination}.")
        elif not self.is_valid_keboola_destination(looker_object.output) and len(looker_object.output) == 0:
            destination = f"in.c-looker.looker_data_{looker_object.id}"
            logging.debug(f"The table with id {looker_object.id} will be saved to {destination}.")
        else:
            raise UserException(f"The name of the table {looker_object.output} contains unsupported characters. "
                                f"Please provide a valid name with bucket and table name.")
        return destination

    def download_look(self, look_id, limit=5000):
        try:
            return self.client.download_look(look_id=look_id, limit=limit)
        except LookerClientException as e:
            raise UserException(f"Failed to download look {look_id}. Validate that it is a valid look ID") from e

    @staticmethod
    def extract_subdomain(url):
        start = len('https://')
        end = url.find('.looker.com')
        return url[start:end]

    @staticmethod
    def lowercase_csv_header(csv_string: str) -> str:
        """Convert the first row (header) to lowercase"""
        csv_list = list(csv.reader(csv_string.splitlines()))

        if csv_list:
            csv_list[0] = [header.lower() for header in csv_list[0]]

        csv_with_lowercase_header = "\n".join([",".join(row) for row in csv_list])

        return csv_with_lowercase_header

    def download_look_object(self, looker_object: LookerObject):
        primary_keys = comma_separated_values_to_list(looker_object.primary_key)
        table_definition = self.create_out_table_definition(f'looker_data_{looker_object.id}.csv',
                                                            destination=self.get_destination(looker_object),
                                                            incremental=looker_object.incremental,
                                                            primary_key=primary_keys)

        look_data = self.download_look(looker_object.id, limit=looker_object.limit)

        # make header lowercase to use the same header as v3 API
        look_data = (self.lowercase_csv_header(look_data))

        with open(table_definition.full_path, "w") as file:
            file.write(look_data)

        self.write_manifest(table_definition)


if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
